import {Row, Col, Container} from 'react-bootstrap';
import { useState } from 'react';
import Iframe from 'react-iframe';

function LocationAndHours () {
  return (
    <Container>
      <Row>
        <Col>
          <div className="col-6">
            <h1> Visit us Here: </h1>
          </div>
          <div className="col-6 d-flex align-items-center justify-content-center">
                <Iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3905.839230552131!2d124.87978241149615!3d11.776368139904491!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33084a94eeb5a3e1%3A0xed4ac0e761d3acb7!2sFlorific%20Food%20Haus!5e0!3m2!1sen!2sph!4v1686134053959!5m2!1sen!2sph"
      width="600"
      height="450"
      frameBorder="0"
      style={{ border: 0 }}
      allowFullScreen=""
      loading="lazy"
      referrerPolicy="no-referrer-when-downgrade"
    />
          </div>
        </Col>
        <Col>
        <h1>Store Hours:</h1>
        <div>Monday: 9am-6pm</div>
        <div>Tuesday: 9am-6pm</div>
        <div>Wednesday: 9am-6pm</div>
        <div>Thursday: 9am-6pm</div>
        <div>Friday: 9am-6pm</div>
        <div>Saturday: 9am-5:30pm</div>
        <div>Sunday: CLOSED</div>

        </Col>
      </Row>
    </Container>
  )
    
}

export default LocationAndHours;