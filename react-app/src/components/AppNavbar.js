// To use react bootstrap components, you must first import them from the react-bootstrap package
import { Fragment, useState, useContext } from 'react';
import {Container, Navbar, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { Link as ScrollLink, animateScroll as scroll } from 'react-scroll';

export default function AppNavbar(){
  // Gets the user email after the user has logged in
  const { user } = useContext(UserContext)

  return(
    <>
      <Navbar>
        <Container className="navbar font-link">
          <Nav className="me-auto">
            <Nav.Link as= {NavLink} to= "/">Home</Nav.Link>
            <Nav.Link as= {NavLink} to= "/products/active">Menu</Nav.Link>
             
             { (user.isAdmin) ?
              <Nav.Link as= {NavLink} to="/admin">Admin Dashboard</Nav.Link>
              :
              <Nav.Link as= {NavLink} to="/location">Location & Hours</Nav.Link>
            }
           {  (user.id !== null) ?
             <Nav.Link as= {NavLink} to="/logout">Logout</Nav.Link>
             :
             <Fragment>
            <Nav.Link as= {NavLink} to="/login">Log In</Nav.Link>
            <Nav.Link as= {NavLink} to="/register">Register</Nav.Link>
            </Fragment>
           }

          </Nav>
        </Container>
      </Navbar>
    </>
  )
}
