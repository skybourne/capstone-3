import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2'

export default function UpdateProductModal({ isOpen, closeModal, product }) {
  const { _id, name, description, price } = product;

  const updateProduct = (productId, updatedProduct) => {
    const token = localStorage.getItem('token');

    return fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(updatedProduct),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.error) {
          Swal.fire({
            title: 'Oopsie daisy',
            icon: 'error',
            text: 'Something went wrong :(',
          });
        } else {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: data.message,
          });
        }
      })
      .catch((error) => {
        console.error('Error updating product:', error);
        throw error;
      });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const updatedProduct = {
      name: event.target.elements.name.value,
      description: event.target.elements.description.value,
      price: event.target.elements.price.value,
    };

    updateProduct(_id, updatedProduct)
      .then((data) => {
        // Handle the successful update
        closeModal();
      })
      .catch((error) => {
        // Handle the error
        Swal.fire({
          title: 'Oopsie daisy',
          icon: 'error',
          text: error.message,
        });
      });
  };

  return (
    <Modal show={isOpen} onHide={closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>Update Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group>
            <Form.Label>Product Name:</Form.Label>
            <Form.Control
              type="text"
              name="name"
              defaultValue={name}
              placeholder="Name input"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Product Description:</Form.Label>
            <Form.Control
              type="text"
              name="description"
              defaultValue={description}
              placeholder="Description input"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Price:</Form.Label>
            <Form.Control
              type="text"
              name="price"
              defaultValue={price}
              placeholder="Price input"
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}
