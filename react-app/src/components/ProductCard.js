import { useState, useEffect } from 'react';
import {Card, Button, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({product}){
  // You can destructure props twice to be able to access their properties directly without using dot (.) notation in the JSX code.
  const {_id, name, description, price} = product 


  return(
      <Card className="my-3 col-6 d-flex"> 
        <Card.Body className="d-flex flex-column justify-content-between">
          <div className="mb-4">
            <Card.Title>{name}</Card.Title>

            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>

            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>Php {price}</Card.Text>
          </div>

          <Link className="btn btn-primary" to={`/products/${_id}/view`}>View Product</Link>
        </Card.Body>
      </Card>
  )
}

// You can use prop types to validate the data from props before rendering the JSX
ProductCard.propTypes = {
  product: PropTypes.shape({ //shape() function dictates the shape/structure of the props
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
