import Carousel from 'react-bootstrap/Carousel';

function Banner() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="carousel1.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>Floriffic Food Haus</h3>
          <a className='btn btn-outline-light btn-lg' href='#!' role='button'>
                What's New?
              </a>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="carousel2.png"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3 className="text-dark">We Now Deliver!</h3>
                    <a className='btn btn-outline-dark btn-lg' href='#!' role='button'>
                Order Now
              </a>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="carousel3.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Try Our Desserts!</h3>
          <a className='btn btn-outline-light btn-lg' href='#!' role='button'>
                Order Now!
              </a>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Banner;