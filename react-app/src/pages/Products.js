import {useState, useEffect } from 'react'
import ProductCard from '../components/ProductCard'
import LoadingSpinner from '../components/LoadingSpinner'
import { Row, Col, Container } from 'react-bootstrap'

export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	// Will run upon the initial rendering of the 'Products' component since there are no values on the 2nd argument which is the array.
	useEffect(() => {
		// Sets the isLoading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(result => {

			// Sets the isLoading state to false
			setIsLoading(false)

			setProducts(result.activeProducts.map(product => {
				return (
					<ProductCard key={product._id} product={product}/>
				)
			}))
		})
	}, [])

	return(
		<Container>
		<Row>
			<Col className="d-flex flex-wrap">
				{/*Using the 'products' variable which will return the components to be rendered based on the data that was looped.*/}
				{/* Checks the value of isLoading state to determine wether to render the spinner or the actual data itself.*/}
				{ isLoading ?
						<LoadingSpinner/>
					:
				 		products 
				}
			</Col>
		</Row>
		</Container>
	)
}
