import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function CreateProduct(){
  const navigate = useNavigate()

  const { user } = useContext(UserContext)

  const [productName, setProductName] = useState('')
  const [productDescription, setProductDescription] = useState('')
  const [productPrice, setProductPrice] = useState()
  const [isActive, setIsActive] = useState(false) // For the button conditional rendering



  function createNew(event){
    event.preventDefault()

  fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: productName,
        description: productDescription,
        price: productPrice 
      })
    })
    .then(response => response.json())
    .then(result => {
      if(typeof result !== "undefined"){

          Swal.fire({
              title: "Product Created Succesfully!",
              icon: "success",
              text: "Let's sell this product"
            })
      navigate("/admin")    
      }
    }).catch(error => {
      Swal.fire({
        title: "Something went wrong",
        icon: "error",
        text: error.message
      })
    })
  }

  useEffect(() =>{
    if(productName !== '' && productDescription !== '' && productPrice !== null){
      setIsActive(true)
    } else {
      setIsActive(false)
    }

  }, [productName, productDescription, productPrice])

  return(
    <Row>
      <Col>
        <Form onSubmit={(event) => createNew(event)}>
          <h1>Create a New Product</h1>
              <Form.Group controlId="productName">
                  <Form.Label>Product Name</Form.Label>
                  <Form.Control 
                      type="text" 
                      placeholder="Enter product name" 
                      value={productName}
                      onChange={event => setProductName(event.target.value)}
                      required
                  />
              </Form.Group>

              <Form.Group controlId="productDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control 
                      type="text" 
                      placeholder="Enter product description"
                      value={productDescription}
                      onChange={event => setProductDescription(event.target.value)} 
                      required
                  />
              </Form.Group>

              <Form.Group controlId="productPrice">
                  <Form.Label>Price</Form.Label>
                  <Form.Control 
                      type="number" 
                      placeholder="Enter Price"
                      value={productPrice}
                      onChange={event => setProductPrice(event.target.value)} 
                      required
                  />
              </Form.Group>

          { isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
                              Submit
                  </Button>
                :
                  <Button disabled variant="primary" type="submit" id="submitBtn">
                              Submit
                  </Button> 
          }

          </Form>
      </Col>
    </Row>
  )
}
