import Banner from '../components/Banner';
import LocationAndHours from '../components/LocationAndHours'
import { Link as ScrollLink, animateScroll as scroll } from 'react-scroll';

export default function Home(){
	return (
		<>
		<Banner />
		<LocationAndHours />
		</>
	)
}
