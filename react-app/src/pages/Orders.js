import { useState, useEffect } from 'react';
import OrderCard from '../components/OrderCard';
import LoadingSpinner from '../components/LoadingSpinner';
import { Row, Col, Container } from 'react-bootstrap';

export default function Orders() {
  const [orders, setOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    fetch(`${process.env.REACT_APP_API_URL}/users/allusers`)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);

        setOrders(result.map(user => (
          <OrderCard key={user._id} user={user} />
        )));
      });
  }, []);

  return (
    <Container>
      <Row>
        <Col className="d-flex flex-wrap">
          {isLoading ? (
            <LoadingSpinner />
          ) : (
            orders
          )}
        </Col>
      </Row>
    </Container>
  );
}