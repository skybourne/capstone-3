import {useState, useEffect, Fragment} from 'react'
import LoadingSpinner from '../components/LoadingSpinner'
import { Row, Col, Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import { Link } from 'react-router-dom';
import AdminProductCard from '../components/AdminProductCard'

export default function AdminDashBoard(){
  const [products, setProducts] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  // Will run upon the initial rendering of the 'Products' component since there are no values on the 2nd argument which is the array.
  useEffect(() => {
    // Sets the isLoading state to true
    setIsLoading(true)

    fetch(`${process.env.REACT_APP_API_URL}/products`)
    .then(response => response.json())
    .then(result => {

      // Sets the isLoading state to false
      setIsLoading(false)

      setProducts(result.products.map(product => {
        return (
          <AdminProductCard key={product._id} product={product}/>
        )
      }))
    })
  }, [])

  return(
    <>
    <h1>Admin Dashboard</h1>
    <Link className="btn btn-primary mx-2" to="/products/create">Create a Product</Link>
    {/*<Link className="btn btn-success" to="/users/allusers">View Orders</Link>*/}
    <Row>
      <Col>
        {/*Using the 'products' variable which will return the components to be rendered based on the data that was looped.*/}
        {/* Checks the value of isLoading state to determine wether to render the spinner or the actual data itself.*/}
        { isLoading ?
            <LoadingSpinner/>
          :
            products 
        }
      </Col>
    </Row>
    </>
  )
}
