// Importables
import {Fragment, useState} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import LocationAndHours from './components/LocationAndHours.js'
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Orders from './pages/Orders.js';
import NotFound from './pages/NotFound.js';
import Admin from './pages/Admin.js';
import CreateProduct from './pages/CreateProduct.js'
import ProductView from './components/ProductView.js';
import OrderCard from './components/OrderCard.js';
import {Container} from 'react-bootstrap';
import './App.css';
import { UserProvider } from './UserContext.js';
import 'bootstrap/dist/css/bootstrap.min.css'

// Component function
function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null 
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  // The component function returns JSX syntax that serves as the UI of the component.
  // Note: JSX syntax may look like HTML but it is actually Javascript that is formatted to look like HTML and is not actually HTML. The benefit of JSX is the ability to easily integrate Javascript with HTML syntax.
  return (
    // When rendering multiple components, they must always be enclosed in a parent component/element.
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      {/* The 'Router' initializes that dynamic routing will be involved. */}
        <AppNavbar/>
        <Container fluid={true} className="p-0">
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products/active" element={<Products/>}/>
            <Route path="/products/:productId/view" element={<ProductView/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/admin" element={<Admin/>}/>
            <Route path="/location" element={<LocationAndHours/>}/>
            <Route path="/products/create" element={<CreateProduct/>}/>
            <Route path="/users/allusers" element={<Orders/>}/>
            <Route path="*" element={<NotFound/>}/> 

            {/*
              ACTIVITY (1hr) 8:40PM
              -Create a route which will handle any route that is not defined above.
              -That route will then load a 'NotFound' page which will have the following elements:
                a. h1 (Title)
                b. p (content)
                c. Link (from react-router) which will go back to the previous page
            */}

          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

// Exporting of the component function
export default App;
